
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'POST',

	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		title: 'Create To Do List Item',
		userId: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));

